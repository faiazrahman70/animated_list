import React from 'react'
import {StyleSheet, Text, View} from 'react-native'
import DataFetch from './src/screens/DataFetch'
import {ContextProvider} from './src/context/ContextProvider'

function App() {
  return (
    <ContextProvider>
      <DataFetch />
    </ContextProvider>
  )
}

const styles = StyleSheet.create({})

export default App
