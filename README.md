# Fetching and Animating a list using Custom Hooks

## Animated API

This code uses Animated API to animate posts when a particular post is deleted.

## Custom Hook

In this code custom hook useFetch is implemented where it keeps track of data, data loading and errors.

## Context API

This code uses Context API to maintain the state globally throughout the application ...
