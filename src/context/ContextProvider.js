import React, {useEffect, useState, useContext, createContext} from 'react'
import {useFetch} from '../customHooks/useFetch'

export const MyContext = createContext()

export const ContextProvider = ({children}) => {
  const [renderData, setRenderData] = useState()
  const {data, loading} = useFetch()

  useEffect(() => {
    if (data) {
      setRenderData(data)
    }
  }, [data])

  return (
    <MyContext.Provider
      value={{
        myValue: 'Nafee_',
        renderData,
        setRenderData,
        loading,
        deleteStack: id => {
          let tmp = renderData.filter(val => val.id !== id)
          setRenderData(tmp)
        },
      }}>
      {children}
    </MyContext.Provider>
  )
}
