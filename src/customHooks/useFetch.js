import React, {useEffect, useState} from 'react'
import {data_fetch_url} from '../constants'

const useFetch = () => {
  const [data, setData] = useState(null)
  const [loading, setLoading] = useState(null)
  const [error, setError] = useState(null)

  const fetchData = async () => {
    try {
      const response = await fetch(data_fetch_url)

      const json_ = await response.json()
      setData(json_)
      // console.log(json_)

      setLoading(false)
    } catch (error) {
      console.log(error)
      setError(error)
    }
  }

  useEffect(() => {
    setLoading(true)

    fetchData()
  }, [])

  return {data, loading, error}
}
export {useFetch}
