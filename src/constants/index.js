import {Dimensions} from 'react-native'

export const dim = {
  height: Dimensions.get('window').height,
  width: Dimensions.get('window').width,
}

export const data_fetch_url = 'https://fakestoreapi.com/products/'
