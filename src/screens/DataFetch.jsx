import React, {useEffect, useRef, useState, useContext} from 'react'
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  Animated,
  Button,
  StyleSheet,
  LayoutAnimation,
  UIManager,
} from 'react-native'
import {dim} from '../constants'
import {MyContext} from '../context/ContextProvider'

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true)

export default function DataFetch() {
  //   const dataForFlatlist = [1, 2, 3, 4, 5, 7, 8, 9, 10]

  const [deletedSelected, setDeletedSelected] = useState()

  const {renderData, setRenderData, loading, deleteStack} =
    useContext(MyContext)
  const animationRef = useRef({}).current

  useEffect(() => {
    console.log('loading', loading)
  }, [loading])

  useEffect(() => {
    if (renderData) {
      console.log(renderData.length)
    }
  }, [renderData])

  const renderItem = ({item, index}) => {
    const scale = new Animated.Value(1)

    const shrink = index => {
      Animated.timing(scale, {
        toValue: 0.5,
        duration: 1000,
        useNativeDriver: true,
      }).start(() => {
        let tmp = renderData.filter((item, ind) => ind !== index)
        setRenderData(tmp)

        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring)
      })
    }

    const moveLeft = scale.interpolate({
      inputRange: [0.8, 1],
      outputRange: [-1000, 0],
    })

    // const takePosition = () => {
    //   let animations = []
    //   const translateYValue = new Animated.Value(0)

    //   for (let i = deletedSelected + 1; i < renderData.length; ++i) {
    //     animations.push(
    //       Animated.timing(translateYValue, {
    //         toValue: dim.height * -0.39,
    //         duration: 1000,
    //         useNativeDriver: true,
    //       }),
    //     )
    //     animationRef[deletedSelected] = translateYValue
    //   }

    //   Animated.sequence(animations).start(() => {
    //     deleteStack(index)

    //     const updatePosts = [...renderData]
    //     for (let i = deletedSelected + 1; i < renderData.length; ++i) {
    //       updatePosts[i - 1] = updatePosts[i]
    //     }
    //     updatePosts.pop()

    //     setRenderData(updatePosts)
    //   })
    // }

    return (
      <Animated.View
        style={[
          styles.card,
          deletedSelected === index
            ? {
                transform: [
                  {scale: scale},
                  {
                    translateX: moveLeft,
                  },
                ],
              }
            : deletedSelected > index
            ? {
                translateY: animationRef[index],
              }
            : {},
          ,
        ]}>
        <Text style={styles.cardTitle}>
          {item.id} - {item.title}
        </Text>
        <Text
          style={{
            marginBottom: 15,
            fontSize: 15,
            color: 'black',
            fontWeight: '800',
          }}>
          Quantity - {item.rating.count}
        </Text>
        <Button
          title="Delete"
          onPress={() => {
            setDeletedSelected(index)

            shrink(index)
          }}
        />
      </Animated.View>
    )
  }

  return (
    <View style={styles.dataFetchContainer}>
      {loading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator color={'blue'} size={'large'} />
        </View>
      ) : (
        <FlatList
          data={renderData}
          showsVerticalScrollIndicator={false}
          renderItem={renderItem}
          contentContainerStyle={{
            paddingTop: 40,
            paddingBottom: 30,
          }}
        />
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  card: {
    height: dim.height * 0.39,
    width: dim.width * 0.8,
    backgroundColor: '#c7ff6c',
    marginBottom: 25,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    elevation: 10,
  },
  cardTitle: {
    fontFamily: 'monospace',
    fontSize: 20,
    marginBottom: 10,
    color: 'black',
    fontWeight: '700',
    textAlign: 'justify',
  },
  dataFetchContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
