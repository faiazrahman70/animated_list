import {View, Text, Button, ScrollView, StyleSheet} from 'react-native';
import React from 'react';
import {dim} from '../constants';

export default function Home() {
  const moveLeft = () => {};
  const moveRight = () => {};

  const CardsNumber = [1, 2, 3, 4];

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <View style={styles.header}>
        <Button title="Left"></Button>
        <Button title="Right"></Button>
      </View>

      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{
          padding: 35,
          gap: 40,
        }}>
        {CardsNumber.map((item, ind) => {
          return (
            <View
              key={ind}
              style={{
                height: dim.height * 0.5,
                width: dim.width * 0.85,
                backgroundColor: 'orange',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 15,
              }}>
              <Text
                style={{
                  fontSize: 26,
                  color: 'white',
                  fontFamily: 'monospace',
                }}>
                Card
              </Text>
            </View>
          );
        })}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    height: '20%',
    width: '100%',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    gap: 10,
    flexDirection: 'row',
  },
});
